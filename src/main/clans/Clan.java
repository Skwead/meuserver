package main.clans;

import org.bukkit.Bukkit;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static main.Main.getInstance;

public class Clan {
    private String tag;
    private String name;
    private List<String> members = new ArrayList<>();

    public Clan(String tag, String name, String founder) {
        this.tag = tag;
        this.name = name;
        this.members.add(founder);

        getInstance().getConfig().createSection("Clans."+name);
        getInstance().getConfig().createSection("Clans."+name+".name");
        getInstance().getConfig().createSection("Clans."+name+".tag");
        getInstance().getConfig().createSection("Clans."+name+".members.names");
        getInstance().getConfig().createSection("Clans."+name+".members.uuids");

        getInstance().getConfig().set("Clans."+name+".name", name);
        getInstance().getConfig().set("Clans."+name+".tag", tag);
        getInstance().getConfig().set("Clans."+name+".members.names", this.members);

        for(String nome : getInstance().getConfig().getStringList("Clans."+name+".members.names")){
            List<String> uuids = new ArrayList<>();
            uuids.add(Bukkit.getPlayer(nome).getUniqueId().toString());

            getInstance().getConfig().set("Clans."+name+".members.uuids", uuids);
        }

        getInstance().saveConfig();
        getInstance().getRelConfig().reloadConfigOnTheGo();
    }

    public String getTag() {
        return tag;
    }

    public String getName() {
        return name;
    }

    public List<String> getMembers() {
        return members;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public void setName(String name) {
        this.name = name;
    }
}
