package main.clans;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class ClanCmd implements CommandExecutor {

    List names = new ArrayList<String>();
    List clans = new ArrayList<Clan>();

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        if(cmd.getName().equalsIgnoreCase("clan")){
            if(args[0].equalsIgnoreCase("criar")){
                if(args.length<3){
                 sender.sendMessage(ChatColor.RED+"[ERRO]: argumentos insuficientes.");
                 return true;
                }
                if((names.contains(args[2]))){
                    sender.sendMessage(ChatColor.RED+"[ERRO]: clan já criado.");
                    return true;
                }
                clans.add(new Clan(args[1], args[2], ((Player)sender).getName()));
                sender.sendMessage(ChatColor.GREEN+"Clan"+ChatColor.YELLOW+" ["+args[1]+"] "+args[2]+ChatColor.GREEN+" criado com sucesso!");
                return true;
            }
            return true;
        }
        return false;
    }
}
