package main.chat;
//ChatColor.translateAlternateColorCodes('&', )
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.*;

import static main.Main.getInstance;

public class Chat implements Listener, CommandExecutor{

    public Map<Player, Channel> focused     = new HashMap<>();
//    private List<Player> muted               = new ArrayList<>();

    public Channel global   = new Channel("global", "[G]", ChatColor.GRAY);
    public Channel local    = new Channel("local", "[L]", ChatColor.YELLOW);

    @EventHandler
    public void onChat(AsyncPlayerChatEvent e){
        Channel active = focused.get(e.getPlayer());

        if(active.equals(local)){
            short next=0;
            for(Player recipient : Bukkit.getOnlinePlayers()){
                if(e.getPlayer().getLocation().distance(recipient.getLocation())<30){
                    next++;
                    local.message(recipient, e.getMessage());
                }
            }
            if(next == 1){
                e.getPlayer().sendMessage(ChatColor.YELLOW+"ISOLAAAADOOOO.");
            }

            e.getRecipients().clear();
            return;
        }
        active.message(e.getPlayer(), e.getMessage());
        e.getRecipients().clear();
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e){

        if(Bukkit.getOnlinePlayers().toArray().length == 1){
            getInstance().getServer().getConsoleSender().sendMessage("Vaziu");

            Object[] online = Bukkit.getOnlinePlayers().toArray();
            getInstance().getCm().addRecipient(((Player)online[0]).getName(), local);
        }
        focused.put(e.getPlayer(), global);
        global.addRecipients(e.getPlayer());
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent e){
        focused.remove(e.getPlayer());
        global.removeRecipients(e.getPlayer());
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        String mensagem = "";

        if(command.getName().equalsIgnoreCase("g")){
            if(!(sender instanceof Player)){
                if(args.length == 0){
                    sender.sendMessage(ChatColor.RED+"[ERRO]: consola não pode focar num canal.");
                    return true;
                } else {
                    for(UUID on : global.getRecipients()){
                        Bukkit.getPlayer(on).sendMessage(ChatColor.GRAY+"[G] "+ChatColor.RED+"CONSOLA "+ ChatColor.GRAY+"» "+getInstance().getColorZ().arr2String(args));
                    }
                    return true;
                }
            } else {
                if(args.length == 0){
                    getInstance().getCm().focus((Player) sender, global);
                    return true;
                } else {
                    global.message((Player) sender, getInstance().getColorZ().arr2String(args));
                    return true;
                }
            }
        }
        if(sender instanceof Player){
            Player p = (Player) sender;

            if(command.getName().equalsIgnoreCase("l")){
                if(args.length == 0){
                    getInstance().getCm().focus(p, local);
                    return true;
                } else {
                    short next=0;
                    for(Player recipient : Bukkit.getOnlinePlayers()){
                        if(p.getLocation().distance(recipient.getLocation())<30){
                            next++;
                            local.message(recipient, getInstance().getColorZ().arr2String(args));
                            //recipient.sendMessage(local.getColor()+local.getPrefix()+" "+ChatColor.translateAlternateColorCodes('&', getInstance().getPermMan().getGroup(p))+" "+sender.getName()+" » "+getInstance().getColorZ().arr2String(args));
                        }
                    }
                    if(next==1){
                        p.sendMessage(ChatColor.YELLOW+"ISOLAAAADOOOO.");
                    }
                    return true;
                }
            }
            if(command.getName().equalsIgnoreCase("chat")){
                if(!(args.length<1)){
                    if(args[0].equalsIgnoreCase("criar")){
                        if(args.length == 4){
                            if(getInstance().getColorZ().containsString(args[3])){
                                if(getInstance().getCm().addChannel(new Channel(args[1], args[2], getInstance().getColorZ().chatToColor(args[3])), p)){
                                    getInstance().getCm().focus(p, getInstance().getCm().getChannelByName(args[1]));
                                    p.sendMessage(ChatColor.GREEN+"Canal criado com sucesso.");
                                    return true;
                                }
                                p.sendMessage(ChatColor.RED+"[ERRO]: Canal com esse nome já existe.");
                                return true;
                            }
                            p.sendMessage(ChatColor.RED+"[ERRO]: Cor inválida, use apenas a letra da cor.");
                            return true;
                        }
                        p.sendMessage(ChatColor.RED+"[ERRO]: Número de argumentos inválido.");
                        return false;
                    }
                    if(args[0].equalsIgnoreCase("focar")){
                        if(args.length==2){
                            if((getInstance().getCm().existsChan(getInstance().getCm().getChannelByName(args[1]))&&
                                    (getInstance().getCm().getChannelByName(args[1]).getRecipients().contains(p)))){
                                getInstance().getCm().focus(p, getInstance().getCm().getChannelByName(args[1]));
                                return true;
                            } else {
                                p.sendMessage(ChatColor.RED+"[ERRO]: Canal não encontrado.");
                                return true;
                            }
                        }
                        p.sendMessage(ChatColor.RED+"[ERRO]: Número de arumentos inválido.");
                        return false;
                    }
                    if(args[0].equalsIgnoreCase("convidar")){
                        if(args.length==3){
                            if((Bukkit.getPlayer(args[1]) != null)&&(getInstance().getCm().existsChan(getInstance().getCm().getChannelByName(args[2])))){
                                Bukkit.getPlayer(args[1]).sendMessage(ChatColor.YELLOW+p.getName()+" adicionou-o ao canal "+ args[2]);
                            }
                            p.sendMessage(ChatColor.RED+"[ERRO]: Tipo de argumentos inválido.");
                            return false;
                        } else {
                            p.sendMessage(ChatColor.RED+"[ERRO]: Número de argumentos inválido.");
                            return false;
                        }
                    }
                    if(args[0].equalsIgnoreCase("saír")){
                        if(args.length==2){
                            if(!((args[1].equalsIgnoreCase("global"))||(args[1].equalsIgnoreCase("local")))){
                                if(getInstance().getCm().existsChan(getInstance().getCm().getChannelByName(args[1]))){
                                    getInstance().getCm().getChannelByName(args[1]).removeRecipients(p);
                                    p.sendMessage(ChatColor.GREEN+"Saíu do canal "+getInstance().getCm().getChannelByName(args[1]).getName());
                                    return true;
                                }
                                p.sendMessage(ChatColor.RED+"[ERRO]: Canal não encontrado.");
                                return true;
                            }
                            p.sendMessage(ChatColor.RED+"[ERRO]: Não pode saír desse canal.");
                            return true;
                        }
                        p.sendMessage(ChatColor.RED+"[ERRO]: Número de arugentos inválido.");
                        return false;
                    }
                    p.sendMessage(ChatColor.RED+"[ERRO]: Argumentos inválidos.");
                    return false;
                } else {
                    p.sendMessage(ChatColor.RED+"[ERRO]: argumentos insuficientes.");
                    return false;
                }
            }
        }
        sender.sendMessage(ChatColor.RED+"[ERRO]: Comando reservado a jogadores.");
        return true;
    }

    public Map<Player, Channel> getPlayerChan() {
        return focused;
    }
}