package main.chat;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import java.util.ArrayList;
import java.util.List;

import static main.Main.getInstance;

public class ChatManager implements Listener {
    private List<Channel> channels = new ArrayList<>();
    private List<String> names= new ArrayList<>();
    private List<String> prefixes = new ArrayList<>();

    public boolean addChannel(Channel cc, Player owner){
        if(!((names.contains(cc.getName())) && (prefixes.contains(cc.getPrefix())))){
            names.add(cc.getName());
            prefixes.add(cc.getPrefix());
            channels.add(cc);
            addRecipient(owner.getName(), cc);
            return true;
        } else {
            return false;
        }
    }

    public boolean addChannel(Channel cc){
        if(!((names.contains(cc.getName())) && (prefixes.contains(cc.getPrefix())))){
            names.add(cc.getName());
            prefixes.add(cc.getPrefix());
            channels.add(cc);
            return true;
        } else {
            return false;
        }

    }

    public boolean addRecipient(String nome, Channel c){
        if(Bukkit.getPlayer(nome) != null){
            Player p = Bukkit.getPlayer(nome);
            c.getRecipients().add(p.getUniqueId());
            return true;
        }
        return false;
    }

    public void focus(Player p, Channel c){
        if(channels.contains(c)){
            getInstance().getChat().getPlayerChan().remove(p);
            getInstance().getChat().getPlayerChan().put(p, c);
            p.sendMessage(ChatColor.GREEN+"Conversa focada em "+ChatColor.YELLOW+c.getName());
        } else {
            p.sendMessage(ChatColor.RED+"[Erro]: o canal que quer focar não existe.");
        }
    }

    public Channel getChannelByName(String name){
        for(Channel c : channels){
            if(c.getName().equals(name)){
                return c;
            }
        }
        return null;
    }

    public boolean existsChan(Channel c){
        for(Channel cc : channels){
            if(cc.equals(c)){
                return true;
            }
        }
        return false;
    }

    public List<String> getNames() {
        return names;
    }
}
