package main.chat;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.*;
import static main.Main.getInstance;

public class Channel {

    private String name;
    private String prefix;
    private ChatColor color;
    private Set<UUID> recipients;

    public Channel(String name, String prefix, ChatColor color){
        this.name = name;
        this.prefix = prefix;
        this.color = color;
        this.recipients = new HashSet<>();
    }

    public void message(Player sender, String message){
        for(UUID recipient : this.recipients){
            Bukkit.getPlayer(recipient).sendMessage(this.color+this.prefix+" "
                    +ChatColor.translateAlternateColorCodes('&', getInstance().getPermMan().getGroup(sender)
                    +this.color+" "+sender.getName()+" » "+message));
        }
    }

    public void addRecipients(Player p){
        this.recipients.add(p.getUniqueId());
    }

    public void removeRecipients(Player p){
        this.recipients.remove(p.getUniqueId());
    }

    public String getName() {
        return name;
    }

    public String getPrefix() {
        return prefix;
    }

    public Set<UUID> getRecipients() {
        return recipients;
    }

    public ChatColor getColor() {
        return color;
    }
}
