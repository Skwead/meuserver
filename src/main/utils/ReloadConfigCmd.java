package main.utils;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;

import static main.Main.getInstance;

public class ReloadConfigCmd implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if(command.getName().equalsIgnoreCase("reloadconfig")){
            reloadConfigOnTheGo();
            sender.sendMessage(ChatColor.GREEN+"Config atualizada com sucesso!");
            return true;
        }
        return false;
    }

    public void reloadConfigOnTheGo() {
        getInstance().setMainConfig(YamlConfiguration.loadConfiguration(getInstance().getCFile()));
        getInstance().reloadConfig();
    }
}
