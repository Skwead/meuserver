package main.utils;

import org.bukkit.ChatColor;

public class ColorZ {

    private ChatColor[] colors = {ChatColor.RED, ChatColor.GREEN, ChatColor.AQUA, ChatColor.RED, ChatColor.LIGHT_PURPLE, ChatColor.YELLOW, ChatColor.WHITE,
            ChatColor.BLACK, ChatColor.DARK_BLUE, ChatColor.DARK_GREEN, ChatColor.DARK_AQUA, ChatColor.DARK_RED, ChatColor.DARK_PURPLE, ChatColor.GOLD,
            ChatColor.GRAY, ChatColor.DARK_GRAY};

    private String[] chatColors = {"a", "b", "c", "d", "e", "f", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};

    public ChatColor chatToColor(String s){
        ChatColor ret = ChatColor.RESET;
        switch (s) {
            case "a":
                ret = ChatColor.GREEN;
                break;
            case "b":
                ret = ChatColor.AQUA;
                break;
            case "c":
                ret = ChatColor.RED;
                break;
            case "d":
                ret = ChatColor.LIGHT_PURPLE;
                break;
            case "e":
                ret = ChatColor.YELLOW;
                break;
            case "f":
                ret = ChatColor.WHITE;
                break;
            case "0":
                ret = ChatColor.BLACK;
                break;
            case "1":
                ret = ChatColor.DARK_BLUE;
                break;
            case "2":
                ret = ChatColor.DARK_GREEN;
                break;
            case "3":
                ret = ChatColor.DARK_AQUA;
                break;
            case "4":
                ret = ChatColor.DARK_RED;
                break;
            case "5":
                ret = ChatColor.DARK_PURPLE;
                break;
            case "6":
                ret = ChatColor.GOLD;
                break;
            case "7":
                ret = ChatColor.GRAY;
                break;
            case "8":
                ret = ChatColor.DARK_GRAY;
                break;
            case "green":
                ret = ChatColor.GREEN;
                break;
            case "aqua":
                ret = ChatColor.AQUA;
                break;
            case "red":
                ret = ChatColor.RED;
                break;
            case "light purple":
                ret = ChatColor.LIGHT_PURPLE;
                break;
            case "yellow":
                ret = ChatColor.YELLOW;
                break;
            case "white":
                ret = ChatColor.WHITE;
                break;
            case "black":
                ret = ChatColor.BLACK;
                break;
            case "dark blue":
                ret = ChatColor.DARK_BLUE;
                break;
            case "dark green":
                ret = ChatColor.DARK_GREEN;
                break;
            case "dark aqua":
                ret = ChatColor.DARK_AQUA;
                break;
            case "dark red":
                ret = ChatColor.DARK_RED;
                break;
            case "dark purple":
                ret = ChatColor.DARK_PURPLE;
                break;
            case "gold":
                ret = ChatColor.GOLD;
                break;
            case "gray":
                ret = ChatColor.GRAY;
                break;
            case "dark gray":
                ret = ChatColor.DARK_GRAY;
                break;
        }
        return ret;
    }

    public String colorToChat(ChatColor c){
        String ret = "r";

        if(c.equals(ChatColor.GREEN)){
            ret = "a";
        }
        if(c.equals(ChatColor.AQUA)){
            ret = "b";
        }
        if(c.equals(ChatColor.RED)){
            ret = "c";
        }
        if(c.equals(ChatColor.LIGHT_PURPLE)){
            ret = "d";
        }
        if(c.equals(ChatColor.YELLOW)){
            ret = "e";
        }
        if(c.equals(ChatColor.WHITE)){
            ret = "f";
        }
        if(c.equals(ChatColor.BLACK)){
            ret = "0";
        }
        if(c.equals(ChatColor.DARK_BLUE)){
            ret = "1";
        }
        if(c.equals(ChatColor.DARK_GREEN)){
            ret = "2";
        }
        if(c.equals(ChatColor.DARK_AQUA)){
            ret = "3";
        }
        if(c.equals(ChatColor.DARK_RED)){
            ret = "4";
        }
        if(c.equals(ChatColor.DARK_PURPLE)){
            ret = "5";
        }
        if(c.equals(ChatColor.GOLD)){
            ret = "6";
        }
        if(c.equals(ChatColor.GRAY)){
            ret = "7";
        }
        if(c.equals(ChatColor.DARK_GRAY)){
            ret = "8";
        }
        if(c.equals(ChatColor.GREEN)){
            ret = "9";
        }

        return ret;
    }

    public String arr2String(String[] array){
        String ret = "";
        for(String string : array){
            ret += string;
        }
        return ret;
    }

    public boolean containsString(String color){
        for (String c : chatColors){
            if(c.equalsIgnoreCase(color)){
                return true;
            }
        }
        return false;
    }
}
