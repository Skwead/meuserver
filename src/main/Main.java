package main;

import main.chat.Chat;
import main.chat.ChatManager;
import main.clans.ClanCmd;
import main.permissions.PermissionManager;
import main.permissions.RemGroupCmd;
import main.permissions.SetGroupCmd;
import main.utils.ColorZ;
import main.utils.ColorsCmd;
import main.utils.ReloadConfigCmd;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

public class Main extends JavaPlugin implements Listener {

    private static Main instance;
    public static Main getInstance(){return instance;}

    private PermissionManager permMan = new PermissionManager();
    public PermissionManager getPermMan() {return permMan;}

    private Chat chat = new Chat();
    public Chat getChat(){return chat;}

    private ChatManager cm = new ChatManager();
    public ChatManager getCm(){return this.cm;}

    private ColorZ col = new ColorZ();
    public ColorZ getColorZ(){return col;}

    private ColorsCmd colCmd = new ColorsCmd();
    public ColorsCmd getColCmd(){return colCmd;}

    private ClanCmd clanCmd = new ClanCmd();
    public ClanCmd getClanCmd(){return clanCmd;}

    private ReloadConfigCmd relConfig = new ReloadConfigCmd();
    public ReloadConfigCmd getRelConfig(){return relConfig;}

    FileConfiguration config;
    File cfile;

    public File getCFile(){return cfile;}

    public void setMainConfig(FileConfiguration configFile){
        config = configFile;
    }

    public void onEnable(){
        getServer().getConsoleSender().sendMessage("\n\n" + ChatColor.GREEN + "A ligar!"+ "\n");

        instance = this;

        config = getConfig();
        config.options().copyDefaults(true);
        saveConfig();
        cfile = new File(getDataFolder(), "config.yml");

        this.getCommand("setgroup").setExecutor(new SetGroupCmd());
        this.getCommand("remgroup").setExecutor(new RemGroupCmd());
        this.getCommand("g").setExecutor(chat);
        this.getCommand("l").setExecutor(chat);
        this.getCommand("chat").setExecutor(chat);
        this.getCommand("cores").setExecutor(colCmd);
        this.getCommand("reloadconfig").setExecutor(relConfig);
        this.getCommand("clan").setExecutor(clanCmd);

        this.getServer().getPluginManager().registerEvents(permMan, this);
        this.getServer().getPluginManager().registerEvents(this, this);
        this.getServer().getPluginManager().registerEvents(chat, this);

        getServer().getConsoleSender().sendMessage(ChatColor.BLUE+"\n\nGrupos:\n");
        for (String group : getInstance().getConfig().getConfigurationSection("Permissions.Groups").getKeys(false)) {
            getServer().getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', ChatColor.AQUA + getInstance().getConfig().getString("Permissions.Groups."+group+".tag")));
            getServer().getConsoleSender().sendMessage(ChatColor.AQUA + "Membros:");
            for(String name: getInstance().getConfig().getStringList("Permissions.Groups."+group+".members.names")){
                getServer().getConsoleSender().sendMessage(ChatColor.AQUA + " nome: " + ChatColor.YELLOW + name);
            }
            for(String uuid: getInstance().getConfig().getStringList("Permissions.Groups."+group+".members.UUIDs")){
                getServer().getConsoleSender().sendMessage(ChatColor.AQUA + " UUID: " + ChatColor.YELLOW + uuid);
            }
            for (String permission : getInstance().getConfig().getStringList("Permissions.Groups."+group+".permissions")){
                getServer().getConsoleSender().sendMessage(ChatColor.AQUA+permission+":\n");
            }
        }

        this.getCm().addChannel(this.getChat().global);
        this.getCm().addChannel(this.getChat().local);

        for(Player on: Bukkit.getOnlinePlayers()){
            this.getChat().focused.put(on, this.getChat().global);
            this.getChat().global.addRecipients(on);
        }
    }

    public void onDisable(){
        getServer().getConsoleSender().sendMessage("\n\n" + ChatColor.RED + "A desligar!"+ "\n");
    }


    @EventHandler
    public void onBreak(BlockBreakEvent e){
        if(!e.getPlayer().hasPermission("teste.breakblock")){
            e.setCancelled(true);
            e.getPlayer().sendMessage(ChatColor.RED + "Nein!");
        }
    }
}
