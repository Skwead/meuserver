package main.permissions;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static main.Main.getInstance;

public class SetGroupCmd implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if(command.getName().equalsIgnoreCase("setgroup")){
            if(args.length==2){
                if((Bukkit.getPlayer(args[0]) instanceof Player) && (getInstance().getPermMan().existsGroup(args[1]))){
                    getInstance().getPermMan().setGroup(Bukkit.getPlayer(args[0]) , args[1]);
                    sender.sendMessage(ChatColor.GREEN+"Jogador " + ChatColor.YELLOW + args[0] + ChatColor.GREEN + " adicionado ao grupo " + ChatColor.YELLOW + args[1]);
                    return true;
                }
                sender.sendMessage(ChatColor.RED+"[ERRO]: tipo inválido de argumentos: "+ChatColor.YELLOW+"/setgroup <jogador> <tag>.");
                return true;
            }
            sender.sendMessage(ChatColor.RED+"[ERRO]: número inválido de argumentos: "+ChatColor.YELLOW+"/setgroup <jogador> <tag>.");
            return true;
        }
        return false;
    }
}
