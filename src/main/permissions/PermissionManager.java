package main.permissions;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.permissions.PermissionAttachment;

import java.util.*;

import static main.Main.getInstance;
import static org.bukkit.Bukkit.getServer;

public class PermissionManager implements Listener {
    private Map<UUID, PermissionAttachment> playerPermissions = new HashMap<>();

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        getServer().getConsoleSender().sendMessage(ChatColor.AQUA+"Jogador novo entrou: "+ChatColor.YELLOW+player.getName() +ChatColor.AQUA+ ". UUID: "+ChatColor.YELLOW+player.getUniqueId());
        setupPerms(player);
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        playerPermissions.remove(player.getUniqueId());
    }

    public void setupPerms(Player player){
        for(String group : getInstance().getConfig().getConfigurationSection("Permissions.Groups").getKeys(false)){
            getServer().getConsoleSender().sendMessage(ChatColor.AQUA+"Grupo em verificação: "+ ChatColor.translateAlternateColorCodes('&', ChatColor.YELLOW +getInstance().getConfig().getString("Permissions.Groups."+group+".tag")));

            for(String uuid : getInstance().getConfig().getStringList("Permissions.Groups."+group+".members.UUIDs")) {
                getServer().getConsoleSender().sendMessage(ChatColor.AQUA+"UUID em verificação: "+ ChatColor.YELLOW+uuid);

                if(player.getUniqueId().toString().equals(uuid)){
                    getServer().getConsoleSender().sendMessage(ChatColor.AQUA+"UUID identificado.");

                    PermissionAttachment attachment = player.addAttachment(getInstance());
                    this.playerPermissions.put(player.getUniqueId(), attachment);

                    getServer().getConsoleSender().sendMessage(ChatColor.AQUA+"Adicionado ao playerPermissions.");
                    for(String permission : getInstance().getConfig().getStringList("Permissions.Groups."+group+".permissions")){
                        attachment.setPermission(permission, true);
                        getServer().getConsoleSender().sendMessage(ChatColor.AQUA+"Atribuida permição: "+ChatColor.YELLOW+permission);
                    }

                    getServer().getConsoleSender().sendMessage(ChatColor.AQUA+"Atribuição concluida.");
                    return;
                }
                getServer().getConsoleSender().sendMessage(ChatColor.RED+"Não foi possível identificar o UUID do jogador.");
                return;
            }
        }
    }

    public void setGroup(Player p, String tag){
        for(String group : getInstance().getConfig().getConfigurationSection("Permissions.Groups").getKeys(false)){
            if(getInstance().getConfig().getString("Permissions.Groups."+group+".tag").equals(tag)){

                List<String> names;
                List<String> uuids;

                names = getInstance().getConfig().getStringList("Permissions.Groups."+group+".members.names");
                uuids = getInstance().getConfig().getStringList("Permissions.Groups."+group+".members.UUIDs");

                names.add(p.getName());
                uuids.add(p.getUniqueId().toString());

                getInstance().getConfig().set("Permissions.Groups."+group+".members.names", names);
                getInstance().getConfig().set("Permissions.Groups."+group+".members.UUIDs", uuids);
                getInstance().saveConfig();

                setupPerms(p);

                return;
            }
        }
    }

    public void remGroup(Player p, String tag){
        for(String group : getInstance().getConfig().getConfigurationSection("Permissions.Groups").getKeys(false)){
            if(getInstance().getConfig().getString("Permissions.Groups."+group+".tag").equals(tag)){

                List<String> names;
                List<String> uuids;

                names = getInstance().getConfig().getStringList("Permissions.Groups."+group+".members.names");
                uuids = getInstance().getConfig().getStringList("Permissions.Groups."+group+".members.UUIDs");

                names.remove(p.getName());
                uuids.remove(p.getUniqueId().toString());

                getInstance().getConfig().set("Permissions.Groups."+group+".members.names", names);
                getInstance().getConfig().set("Permissions.Groups."+group+".members.UUIDs", uuids);
                getInstance().saveConfig();

                setupPerms(p);

                return;
            }
        }
    }

    public boolean existsGroup(String group){
        for(String tag: getInstance().getConfig().getConfigurationSection("Permissions.Groups").getKeys(false)){
            if(group.equalsIgnoreCase(getInstance().getConfig().getString("Permissions.Groups."+tag+".tag"))){
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public String getGroup(Player p){
        for (String group : getInstance().getConfig().getConfigurationSection("Permissions.Groups").getKeys(false)) {
            for(String name: getInstance().getConfig().getStringList("Permissions.Groups."+group+".members.names")){
                if(p.getName().equals(name)){
                    return getInstance().getConfig().getString("Permissions.Groups."+group+".tag");
                }
            }
        }
        return "";
    }
}
